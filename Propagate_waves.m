function [Ex,Ey,Ez,Hx,Hy,Hz]=Propagate_waves(wavelength,n,Ex0,Ey0,x,y,d)

eta0=376.73031;
k=2*pi*n/wavelength;
Nx=length(x);
Ny=length(y);

dx=x(2)-x(1);
dy=y(2)-y(1);
fsx=1/dx;
fsy=1/dy;

dfx=fsx/Nx;
dfy=fsy/Ny;

kx=2*pi*(-floor(Nx/2)*dfx:dfx:(Nx-floor(Nx/2)-1)*dfx);
ky=2*pi*(-floor(Ny/2)*dfy:dfy:(Ny-floor(Ny/2)-1)*dfy);

kx2D=repmat(kx,length(ky),1);
ky2D=repmat(ky',1,length(kx));
kz2D=sqrt(k^2-kx2D.^2-ky2D.^2);


Ex_tilde=fftshift(fft2(Ex0)).*exp(-1i*conj(kz2D)*d);
Ey_tilde=fftshift(fft2(Ey0)).*exp(-1i*conj(kz2D)*d);
Ez_tilde=-(Ex_tilde.*kx2D+Ey_tilde.*ky2D)./kz2D;

Hx_tilde=-wavelength/(2*pi*eta0)*(Ey_tilde.*kz2D-Ez_tilde.*ky2D);
Hy_tilde=-wavelength/(2*pi*eta0)*(Ez_tilde.*kx2D-Ex_tilde.*kz2D);
Hz_tilde=-wavelength/(2*pi*eta0)*(Ex_tilde.*ky2D-Ey_tilde.*kx2D);


Ex=ifft2(ifftshift(Ex_tilde));
Ey=ifft2(ifftshift(Ey_tilde));
Ez=ifft2(ifftshift(Ez_tilde));

Hx=ifft2(ifftshift(Hx_tilde));
Hy=ifft2(ifftshift(Hy_tilde));
Hz=ifft2(ifftshift(Hz_tilde));
