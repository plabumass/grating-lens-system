using Roots
include("./GratingLenses.jl")

function pointonsecondsurface(origin::GratingLenses.Point,
                              first::GratingLenses.Point,
                              second::GratingLenses.Point,
                              image::GratingLenses.Point,
                              pathlength::Real)
    r1 = origin.r
    z1 = first.z - origin.z
    d = second.z - first.z
    z2 = image.z - second.z
    f(r2) = sqrt(r1^2 + z1^2) + sqrt((r1-r2)^2+d^2) + sqrt(r2^2+z2^2) - pathlength
    return GratingLenses.Point(find_zero(f, 0), second.z)
end

Z1 = 4.0
D = 1.0
Z2 = 2.0
K = 2*pi/830e-3

porigin = GratingLenses.Point(0, 0)
pfirst = GratingLenses.Point(Z1, 4)
psecond = GratingLenses.Point(Z1+D, 0)
pimage = GratingLenses.Point(Z1+D+Z2, 0)
pathlength = 20.0

pcalc = pointonsecondsurface(porigin, pfirst, psecond, pimage, pathlength)
ray1 = GratingLenses.Ray(K, porigin, pfirst)
ray2 = GratingLenses.Ray(K, pfirst, pcalc)
ray3 = GratingLenses.Ray(K, pcalc, pimage)
surf1 = GratingLenses.RadialPhaseSurface()
surf2 = GratingLenses.RadialPhaseSurface()
GratingLenses.definesurface!(surf1, ray1, ray2)
GratingLenses.definesurface!(surf2, ray2, ray3)


