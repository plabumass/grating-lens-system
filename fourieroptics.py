# https://customers.zemax.com/os/resources/learn/knowledgebase/how-to-design-diffractive-optics-using-the-binary

import numpy as np

# througout the units used are microns
using_fftw = False
using_numpy = True

if using_numpy:
    from numpy.fft import fftshift, ifftshift, fft2, ifft2, fftfreq
elif using_fftw:
    from pyfftw.interfaces.numpy_fft import fftshift, ifftshift, fft2
    from pyfftw.interfaces.numpy_fft import ifft2, fftfreq


class OpticalSystem:
    """
    This yet-unimplemented class defines an optical system consisting of a
    start field, locations of optical elements (lenses and apertures) and an
    "image" field (can be defocused).
    """
    pass


class ScalarField:
    def __init__(self, width=200.0, resolution=1, wavelength=0.5):
        # there should be some warning here about resolution/wavelength;
        # wl*fX needs to be less than 1
        """Initializes field object."""
        self.width = width
        self.resolution = resolution
        self.wavelength = wavelength
        self.propagation = 2*np.pi/self.wavelength  # |k|
        # field assumed square, and initialized to be empty
        self.x = np.arange(start=-self.width/2,
                           stop=self.width/2,
                           step=self.resolution)
        self.y = np.arange(start=-self.width/2,
                           stop=self.width/2,
                           step=self.resolution)
        nx = len(self.x)
        ny = len(self.y)
        self.fx = fftshift(fftfreq(nx, d=self.resolution))
        self.fy = fftshift(fftfreq(ny, d=self.resolution))
        self.field = np.zeros(shape=(nx, ny), dtype=np.complex128)

    def apply_distribution_space(self, fun):
        """Applies fun image space to create field distribution."""
        # X, Y = np.meshgrid(self.x, self.y)
        # Z = fun(X.astype(np.complex128), Y.astype(np.complex128))
        Z = fun(self.x, self.y)
        self.field = Z.astype(np.complex128)

    def apply_distribution_angle(self, fun):
        fX, fY = np.meshgrid(self.fx, self.fy)
        field = fun(fX.astype(np.complex128), fY.astype(np.complex128))
        self.field = ifftshift(ifft2(field))

    def apply_element(self, element, aperture=None):
        """Interacts field with element (e.g., a lens)."""
        T = element.transfer(self.wavelength, self.x, self.y)
        if aperture is not None:
            X, Y = np.meshgrid(self.x, self.y)
            M = np.zeros(X.shape)
            M[X**2 + Y**2 < aperture**2] = 1
            T *= M
        self.field *= T.astype(np.complex128)

    def apply_plane_wave(self, angle_x=0.0):
        """Initializes self.field to uniform plane wave, possibly at an angle
        from the normal.
        """
        def angle_field(x, y):
            X, _ = np.meshgrid(x, y)
            k = self.propagation
            return np.exp(1j*k*X*np.sin(angle_x))

        self.apply_distribution_space(angle_field)

    def apply_spherical_wave(self, z, x=0, y=0):
        """Initializes self.field to a spherical wave (point source)."""
        def sphere_field(u, v):
            U, V = np.meshgrid(u, v)
            U -= x
            V -= y
            r = np.sqrt(z**2 + U**2 + V**2)
            k = self.propagation
            return np.exp(1j*k*r)/r

        self.apply_distribution_space(sphere_field)

    def get_intensity(self):
        """Returns intensity distribution."""
        return np.abs(self.field)**2

    def initialize_disk(self, x=0, y=0, radius=10):
        def disk(u, v):
            if np.abs(u-x)**2 + np.abs(v-y)**2 < radius**2:
                return 1.0 + 0.0j
            else:
                return 0.0 + 0.0j

        self.apply_distribution(np.vectorize(disk))

    def initialize_gauss(self, x=0, y=0, sigma=10):
        def gaussian(u, v):
            return np.exp(-((u-x)**2 + (v - y)**2)/(2*sigma**2))

        self.apply_distribution(np.vectorize(gaussian))

    def propagate(self, distance):
        """Propagates field by distance."""
        fX, fY = np.meshgrid(self.fx, self.fy)
        fourier_field = fftshift(fft2(self.field))
        k = self.propagation
        z = distance
        wl = self.wavelength
        sarg = 1-(wl*fX)**2-(wl*fY)**2
        mask = np.exp(1j*k*z*np.sqrt(sarg.astype(np.complex128)))
        fourier_field *= mask
        fourier_field = ifftshift(fourier_field)
        self.field = ifft2(fourier_field)


class Element:
    def __init__(self):
        """Initializes element."""
        pass

    def transfer(self, wavelength, x, y):
        """Generates phase transformation at wavelength."""
        # right now just serves as a template to be overwritten for specific
        # elements.
        pass


class ThinLens(Element):
    def __init__(self, material, radii):
        """Initializes lens element."""
        super().__init__()
        self.n = material.refractive_index
        self.radii = radii

    def focal_length(self, wavelength):
        finv = (1/self.radii[0] - 1/self.radii[1])*(self.n(wavelength)-1)
        return 1/finv

    def transfer(self, wavelength, x, y):
        """Generates phase transformation at wavelength."""
        finv = 1/self.focal_length(wavelength)
        k = 2*np.pi/wavelength
        X, Y = np.meshgrid(x, y)
        argument = -1j*k*finv/2*(X**2 + Y**2)
        return np.exp(argument)


class PhaseSurface(Element):
    def __init__(self, function):
        super().__init__()
        self.phase_function = function

    def transfer(self, wavelength, x, y):
        # X, Y = np.meshgrid(x, y)
        # f = np.vectorize(self.phase_function)
        # does not depend on wavelength; see FresnelChromatic.ipynb for
        # justification
        argument = 1j*self.phase_function(x, y)
        return np.exp(argument)


class Grating(PhaseSurface):
    def __init__(self, wavelength, angle, direction=0):
        self.specifications = {"wavelength": wavelength,
                               "angle": angle}
        self.direction = direction
        # sets phase function
        super().__init__(function=self.grating_function)

    def grating_function(self, x, y):
        wavelength = self.specifications["wavelength"]
        angle = self.specifications["angle"]
        k = 2*np.pi/wavelength
        if self.direction == 0:  # x direction
            return k*x*np.sin(angle)
        else:                   # y direction
            return k*y*np.sin(angle)


class DiffractiveLens(PhaseSurface):
    def __init__(self, wavelength, focus, x=0, y=0):
        self.specifications = {"wavelength": wavelength,
                               "focus": focus,
                               "x": x,
                               "y": y}
        super().__init__(function=self.lens_function)

    def lens_function(self, x, y):
        wavelength = self.specifications["wavelength"]
        k = 2*np.pi/wavelength
        focus = self.specifications["focus"]
        x0 = self.specifications["x"]
        y0 = self.specifications["y"]
        u = x - x0
        v = y - y0
        sarg = u**2 + v**2 + focus**2
        return -k*np.sqrt(sarg.astype(np.complex128))


class RadialPhaseSurface(PhaseSurface):
    def __init__(self, coeffs, xoff=0.0, yoff=0.0):
        self.coeffs = coeffs
        self.xoff = xoff
        self.yoff = yoff
        super().__init__(function=self.radial_phase_function)

    def radial_phase_function(self, x, y):
        """
        Phase function for a grating lens, a la Kato 1989.
        
        Arguments:
        x (numpy.array): x coordinates.
        y (numpy.array): y coordinates.
        coeffs (list): radial coefficients in ascending order (r**1, r**2, etc.)
        xoff (float): offset of element w.r.t. the x axis.
        yoff (float): offset of element w.r.t. the y axis.

        Returns:
        Z (numpy.array): 2D array of np.float64 representing spatial phase 
        distribution.
        """
        X, Y = np.meshgrid(x, y)
        X -= self.xoff
        Y -= self.yoff
        R = np.sqrt(X**2 + Y**2)
        Z = np.zeros(X.shape)
        for ix, c in enumerate(self.coeffs):
            Z += c * R**(ix+1)

        Z = np.mod(Z, 2*np.pi)
        return Z


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    from glasses import nbk7

    field = ScalarField(wavelength=0.5, width=400.0, resolution=0.5)
    # f = np.vectorize(lambda x, y: 1 if x**2 + y**2 < 20**2 else 0)
    sigma = 10
    f = np.vectorize(lambda x, y: np.exp(-(x**2 + y**2)/(2*sigma**2)))
    field.apply_distribution(f)
    nx, ny = field.field.shape
    num = 500
    ctr = int(ny/2)
    inty = np.zeros(shape=(nx, 3*num))
    for ix in range(num):
        field.propagate(1.0)
        inty[:, ix] = field.get_intensity()[:, ctr]

    lens = ThinLens(nbk7, (50, -50))
    field.apply_element(lens)

    for ix in range(num, 3*num):
        field.propagate(1.0)
        inty[:, ix] = field.get_intensity()[:, ctr]

    plt.pcolormesh(range(3*num), field.x, inty)
    plt.show()
