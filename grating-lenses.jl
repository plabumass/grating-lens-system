# Kato 1989
using PyPlot
using Polynomials
# TODO: fix definesurface! to auto-detect which ray is incident

# POINTS
struct Point
    r::Real
    z::Real
end

# RAYS
struct Ray
    k::Real                  # propagation constant
    p::Point
    θ::Real                  # incident angle
end

"""
Ray constructor from k and two points
"""
function Ray(k::Real, from::Point, to::Point)
    r = to.r-from.r
    z = to.z-from.z
    θ = atan(r/z)
    return Ray(k, Point(to.r, to.z), θ)
end

# OPTICAL ELEMENTS
abstract type AbstractOpticalElement end

struct Grating <: AbstractOpticalElement
    dpdr::Real               # derivative of phase
end


"""
Grating constructor without spatial phase derivative.
"""
function Grating(k::Real, θi::Real, θt::Real)
    dpdr = k*(sin(θt)-sin(θi))
    return Grating(dpdr)
end

"""
Grating constructor from input and output rays
"""
function Grating(input::Ray, output::Ray)
    if input.k != output.k
        error("Propagation constants do not match.")
    end
    return Grating(input.k, input.θ, output.θ)
end

mutable struct RadialPhaseSurface <: AbstractOpticalElement
    z::Real                     # z position
    r::Array{Real, 1}           # array of radial positions
    dpdr::Array{Real, 1}        # phase derivative at those positions
end

"""
Constructor for empty phase surface
"""
function RadialPhaseSurface(z::Real)
    return RadialPhaseSurface(z, Real[], Real[])
end

# RAY OPERATIONS
# These can probably be condensed into a few functions.
function forwardtransform(ray::Ray, grating::Grating)
    θt = asin(grating.dpdr/ray.k+sin(ray.θ))
    return Ray(ray.k, ray.p, θt)
end

function forwardtransform(ray::Ray, rps::RadialPhaseSurface)
    index = argmin(map(x->abs(x-ray.p.r), rps.r)) # TODO: this is not a safe way of doing this.
    dpdr = rps.dpdr[index]
    grating = Grating(dpdr)
    return forwardtransform(ray, grating)
end

function reversetransform(ray::Ray, grating::Grating)
    θi = asin(sin(ray.θ)-grating.dpdr/ray.k) 
    return Ray(ray.k, ray.p, θi)
end

function reversetransform(ray::Ray, rps::RadialPhaseSurface)
    index = argmin(map(x->abs(x-ray.p.r), rps.r))
    dpdr = rps.dpdr[index]
    grating = Grating(dpdr)
    return reversetransform(ray, grating)
end

function forwardpropagate(ray::Ray, distance::Real)
    r = ray.p.r + tan(ray.θ) * distance
    z = ray.p.z + distance
    return Ray(ray.k, Point(r, z), ray.θ)
end

function reversepropagate(ray::Ray, distance::Real)
    r = ray.p.r - tan(ray.θ) * distance
    z = ray.p.z - distance
    return Ray(ray.k, Point(r, z), ray.θ)
end

function kshift(ray::Ray, k::Real)
    return Ray(k, ray.p, ray.θ)
end

function definesurface!(surf::RadialPhaseSurface, ray1::Ray, ray2::Ray)
    grating = Grating(ray1, ray2)
    push!(surf.r, ray1.p.r)
    push!(surf.dpdr, grating.dpdr)
end

function forwardtrace!(incident::Ray,
                       fixed::RadialPhaseSurface,
                       mutable::RadialPhaseSurface,
                       target::Point)
    distance = mutable.z - fixed.z
    if (incident.p.z == fixed.z) & (distance > 0)
        # 1. propagate through first grating
        ray2 = forwardtransform(incident, fixed)
        ray3 = forwardpropagate(ray2, distance)
        # 2. calculate necessary response of second grating    
        ray4 = Ray(ray3.k, ray3.p, target)
        definesurface!(mutable, ray3, ray4)
        # check that this works!
    else
        println("Whuh-oh.")
    end
end

function reversetrace!(incident::Ray,
                       fixed::RadialPhaseSurface,
                       mutable::RadialPhaseSurface,
                       target::Point)
    distance = fixed.z - mutable.z
    if (incident.p.z == fixed.z) & (distance > 0)
        ray2 = reversetransform(incident, fixed)
        ray3 = reversepropagate(ray2, distance)
        ray4 = Ray(ray3.k, target, ray3.p)
        definesurface!(mutable, ray4, ray3)
    else
        println("Whoops.")      # actually throw error
    end
end

function raytolastsurfacepoint(k::Real, point::Point, surf::RadialPhaseSurface)
    spoint = Point(surf.r[end], surf.z) 
    return Ray(k, point, spoint)
end

struct TwoLensSystem
    object::Point
    edge1::Point                # top of first grating
    center2::Point              # center of second grating (height irr.)
    image::Point
end



function solvesystem(s::TwoLensSystem, k1, k2; plots=false)
    
    d = s.center2.z - s.edge1.z
    l1 = s.edge1.z - s.object.z
    l2 = s.image.z - s.center2.z

    surf1 = RadialPhaseSurface(s.edge1.z)
    surf2 = RadialPhaseSurface(s.center2.z)

    # Forward k1: defines problem
    ray1 = Ray(k1, s.object, s.edge1)
    ray2 = Ray(k1, s.edge1, s.center2)
    definesurface!(surf1, ray1, ray2)
    ray3 = Ray(k1, s.center2, s.image)
    definesurface!(surf2, ray2, ray3)

    ray4 = raytolastsurfacepoint(k2, s.object, surf1)
    ray5 = forwardtransform(ray4, surf1)
    ray6 = forwardpropagate(ray5, d)

    while surf1.r[end] > 0
        # Forward k2
        ray = raytolastsurfacepoint(k2, s.object, surf1)
        forwardtrace!(ray, surf1, surf2, s.image)
        # Reverse k1
        ray = raytolastsurfacepoint(k1, s.image, surf2)
        reversetrace!(ray, surf2, surf1, s.object)
    end

    polyo = 3                   # polynomial order
    f1 = polyfit(surf1.r, surf1.dpdr, polyo)
    f2 = polyfit(map(x->-x+s.center2.r, surf2.r), surf2.dpdr, polyo)

    function plotderivatives()
        figure()
        plot(surf1.r, map(x->x, surf1.dpdr), "o")
        plot(map(x->-x+s.center2.r, surf2.r), map(x->x, surf2.dpdr), "x")

        plot(surf1.r, f1(surf1.r), "k")
        plot(map(x->-x+s.center2.r, surf2.r), f2(map(x->-x+s.center2.r, surf2.r)), "k")
        
        # ylim(0, 10000)
        xlabel("radial position (um)")
        ylabel("phase derivative (rad/um)")
        legend(["GL1", "GL2"])
        grid()
        show()
    end


    # define plot functions
    rvec = (0:0.1:1)*s.edge1.r
    
    function tracerays()
        plength = zeros(size(rvec))        
        function getraytrace(ray)
            ray0 = reversepropagate(ray, l1)
            ray2 = forwardtransform(ray, Grating(f1(abs(ray.p.r))))
            ray3 = forwardpropagate(ray2, d)
            ray4 = forwardtransform(ray3, Grating(f2(map(x->-x+s.center2.r, ray3.p.r))))
            ray5 = forwardpropagate(ray4, l2)
            zv = map(x->x.p.z, [ray0, ray, ray2, ray3, ray4, ray5])
            rv = map(x->x.p.r, [ray0, ray, ray2, ray3, ray4, ray5])
            return zv, rv
        end

        figure()
        for (ix, r) in enumerate(rvec)
            # color1
            ray1 = Ray(k1, s.object, Point(r, s.edge1.z))
            zv, rv = getraytrace(ray1)
            # calculate path length
            plength[ix] = sum(map((dz, dr)->sqrt(dz^2+dr^2),
                                  diff(zv),
                                  diff(rv)))
            plot(zv, rv, "r:")

            #color2
            ray2 = Ray(k2, s.object, Point(r, s.edge1.z))
            zv, rv = getraytrace(ray2)
            plot(zv, rv, "b--")
        end

        # maximum radius
        rm = s.edge1.r
        plot([s.edge1.z, s.edge1.z], [-rm, rm], "k-")
        plot([s.center2.z, s.center2.z], [-rm, rm], "k-")
        legend(["λ₁", "λ₂"])
        grid()
        xlabel("z (mm)")
        ylabel("r (mm)")
        show()

        return plength
    end        

    function plotpathlength(plength)
        # changing path length
        figure()
        plot(rvec, plength, "o-")
        xlabel("r (mm)")
        ylabel("path length (mm)")
        show()
    end

    if plots
        plength = tracerays()
        plotpathlength(plength)
        plotderivatives()
    end
    
    return f1, f2
end

# "main"
tls = TwoLensSystem(Point(0, 0),
                    # Point(1.18e3, 3.75e3),
                    Point(1000, 3.75e3),                    
                    Point(0, 4.75e3),
                    Point(0, 6.45e3))
kshort = 2pi/(825e-3)
klong = 2pi/(835e-3)
poly1, poly2 = solvesystem(tls, kshort, klong; plots=true)

# print coefficients
for polyn in (poly1, poly2)
    for (index, value) in enumerate(polyn.a)
        icoeff = value/index
        icoeff2 = icoeff * (1000)^index
        println("r^$index: $icoeff (um); $icoeff2 (mm)")
    end
end
