module GratingLenses
using PyPlot
using Polynomials
# TODO: fix definesurface! to auto-detect which ray is incident

# POINTS
struct Point
    r::Real
    z::Real
end

# RAYS
struct Ray
    k::Real                  # propagation constant
    p::Point
    θ::Real                  # incident angle
end

"""
Ray constructor from k and two points
"""
function Ray(k::Real, from::Point, to::Point)
    r = to.r-from.r
    z = to.z-from.z
    θ = atan(r/z)
    return Ray(k, Point(to.r, to.z), θ)
end

# OPTICAL ELEMENTS
abstract type AbstractOpticalElement end

struct Grating <: AbstractOpticalElement
    dpdr::Real               # derivative of phase
end


"""
Grating constructor without spatial phase derivative.
"""
function Grating(k::Real, θi::Real, θt::Real)
    dpdr = k*(sin(θt)-sin(θi))
    return Grating(dpdr)
end

"""
Grating constructor from input and output rays
"""
function Grating(input::Ray, output::Ray)
    if input.k != output.k
        error("Propagation constants do not match.")
    end
    return Grating(input.k, input.θ, output.θ)
end

mutable struct RadialPhaseSurface <: AbstractOpticalElement
    z::Real                     # z position
    r::Array{Real, 1}           # array of radial positions
    dpdr::Array{Real, 1}        # phase derivative at those positions
end

"""
Constructor for empty phase surface
"""
function RadialPhaseSurface(z::Real)
    return RadialPhaseSurface(z, Real[], Real[])
end

# RAY OPERATIONS
# These can probably be condensed into a few functions.
function forwardtransform(ray::Ray, grating::Grating)
    θt = asin(grating.dpdr/ray.k+sin(ray.θ))
    return Ray(ray.k, ray.p, θt)
end

function forwardtransform(ray::Ray, rps::RadialPhaseSurface)
    index = argmin(map(x->abs(x-ray.p.r), rps.r)) # TODO: this is not a safe way of doing this.
    dpdr = rps.dpdr[index]
    grating = Grating(dpdr)
    return forwardtransform(ray, grating)
end

function reversetransform(ray::Ray, grating::Grating)
    θi = asin(sin(ray.θ)-grating.dpdr/ray.k) 
    return Ray(ray.k, ray.p, θi)
end

function reversetransform(ray::Ray, rps::RadialPhaseSurface)
    index = argmin(map(x->abs(x-ray.p.r), rps.r))
    dpdr = rps.dpdr[index]
    grating = Grating(dpdr)
    return reversetransform(ray, grating)
end

function forwardpropagate(ray::Ray, distance::Real)
    r = ray.p.r + tan(ray.θ) * distance
    z = ray.p.z + distance
    return Ray(ray.k, Point(r, z), ray.θ)
end

function reversepropagate(ray::Ray, distance::Real)
    r = ray.p.r - tan(ray.θ) * distance
    z = ray.p.z - distance
    return Ray(ray.k, Point(r, z), ray.θ)
end

function kshift(ray::Ray, k::Real)
    return Ray(k, ray.p, ray.θ)
end

function definesurface!(surf::RadialPhaseSurface, ray1::Ray, ray2::Ray)
    grating = Grating(ray1, ray2)
    push!(surf.r, ray1.p.r)
    push!(surf.dpdr, grating.dpdr)
end

function forwardtrace!(incident::Ray,
                       fixed::RadialPhaseSurface,
                       mutable::RadialPhaseSurface,
                       target::Point)
    distance = mutable.z - fixed.z
    if (incident.p.z == fixed.z) & (distance > 0)
        # 1. propagate through first grating
        ray2 = forwardtransform(incident, fixed)
        ray3 = forwardpropagate(ray2, distance)
        # 2. calculate necessary response of second grating    
        ray4 = Ray(ray3.k, ray3.p, target)
        definesurface!(mutable, ray3, ray4)
        # check that this works!
    else
        println("Whuh-oh.")
    end
end

function reversetrace!(incident::Ray,
                       fixed::RadialPhaseSurface,
                       mutable::RadialPhaseSurface,
                       target::Point)
    distance = fixed.z - mutable.z
    if (incident.p.z == fixed.z) & (distance > 0)
        ray2 = reversetransform(incident, fixed)
        ray3 = reversepropagate(ray2, distance)
        ray4 = Ray(ray3.k, target, ray3.p)
        definesurface!(mutable, ray4, ray3)
    else
        println("Whoops.")      # actually throw error
    end
end

function raytolastsurfacepoint(k::Real, point::Point, surf::RadialPhaseSurface)
    spoint = Point(surf.r[end], surf.z) 
    return Ray(k, point, spoint)
end

struct TwoLensSystem
    object::Point
    edge1::Point                # top of first grating
    center2::Point              # center of second grating (height irr.)
    image::Point
end
end
