from numbers import Real
import numpy as np
import fourieroptics as fo
import matplotlib.pyplot as plt
# 150 um aperture
coeffs1 = [-1.0, -2.3e-3, 2.5e-6, 7.6e-9]
coeffs2 = [-1.1, -2.9e-3, -3.4e-6, -1.3e-8]
RMAX = 150.0                    # maximum radius of optics

# 500 um aperture
coeffs1 = [-3.09, -2.05e-3, 6.34e-7, 5.04e-10]
coeffs2 = [-3.41, -2.75e-3, 6.92e-7, 1.2e-9]
RMAX = 500.0                    # maximum radius of optics

# # 1000 um aperture
# coeffs1 = [-5.04, -1.61e-3, 2.04e-7, 7.10e-10]
# coeffs2 = [-5.39, -2.50e-3, 2.14e-7, 2.92e-10]
# RMAX = 1000.0                   # maximum radius of optics

WL = 830e-3
FWID = 1.5*2*RMAX
RES = 0.2
Z1 = 3.75e3
D = 1.0e3
Z2 = 1.7e3

# INITIALIZE FIELD OBJECT
field = fo.ScalarField(wavelength=WL,
                       width=FWID,
                       resolution=RES)
field.apply_spherical_wave(z=Z1)

# APPLY OPERATIONS UNTIL BEFORE FOCUS
lens1 = fo.RadialPhaseSurface(coeffs1)
lens2 = fo.RadialPhaseSurface(coeffs2)
z2v = np.linspace(0.5, 1.5, 100) * Z2
z2d = np.diff(z2v)
z2 = z2v[0]
operations = (lens1, D, lens2, z2)
for op in operations:
    if isinstance(op, Real):
        print("Propagating.")
        field.propagate(op)
    elif isinstance(op, fo.Element):
        print("Applying element.")
        field.apply_element(op, aperture=RMAX)

    plt.imshow(field.get_intensity())
    plt.show()

# INTERROGATE ABOUT FOCUS
zcut = np.zeros(shape=(len(z2d), len(field.y)))
for ix, step in enumerate(z2d):
    field.propagate(step)
    Z = field.get_intensity()
    nx, ny = Z.shape
    zcut[ix, :] = Z[nx//2, :]

plt.pcolormesh(field.y, z2v[1:], zcut)
plt.show()
