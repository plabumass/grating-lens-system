clc				
clear
close all

wavelength=0.83;		% wavelength in microns

x_dipole=0;
y_dipole=0;

z_dipole=150;			% position of dipole

x_offset=0;
y_offset=0;

pixel_size=0.2;			% 200 nm pixel

R_norm=1.0; 			% normalization radius (microns?)

a_lens1=[-2.3e-3 2.8e-10];
a_lens1_radial=2*pi/-0.32;

a_lens2=[-4.6e-3 1.4e-9];
a_lens2_radial=2*pi/-0.45;

z2=1.7e3;
t_substrate=1000.0;



aperture_radius_lens1=150; %31;
aperture_radius_lens2=150; % 498;


n_air=1;
n_substrate=1;

Sx=1000;
Sy=1000;


dx=pixel_size;
dy=pixel_size;
x=-Sx/2:dx:Sx/2;
y=-Sy/2:dy:Sy/2;
[x2D,y2D]=meshgrid(x,y);


r_dipole=sqrt((x2D-x_dipole).^2+(y2D-y_dipole).^2+z_dipole.^2);

%Ex0=exp(-1i*2*pi/wavelength*r_dipole);
Ex0=ones(size(x2D));

Ey0=zeros(size(Ex0));



Ex_before_lens1=Ex0;
Ey_before_lens1=Ey0;


phi_lens1=2*pi/a_lens1_radial*sqrt((x2D-x_offset).^2+(y2D-y_offset).^2);
 
phi_lens2=2*pi/a_lens2_radial*sqrt(x2D.^2+y2D.^2);


for n=1:length(a_lens1)
    phi_lens1=phi_lens1+a_lens1(n)*(sqrt((x2D-x_offset).^2+(y2D-y_offset).^2)/R_norm).^(2*n);
end


for n=1:length(a_lens2)
    phi_lens2=phi_lens2+a_lens2(n)*(sqrt(x2D.^2+y2D.^2)/R_norm).^(2*n);
end



t_lens1=exp(-1i*phi_lens1);
t_lens2=exp(-1i*phi_lens2);

t_mask1=t_lens1.*heaviside(1-sqrt(((x2D-x_offset)/aperture_radius_lens1).^2+((y2D-y_offset)/aperture_radius_lens1).^2));
t_mask2=t_lens2.*(heaviside(1-sqrt((x2D/aperture_radius_lens2).^2+(y2D/aperture_radius_lens2).^2)))+...
    0*heaviside(sqrt((x2D/aperture_radius_lens2).^2+(y2D/aperture_radius_lens2).^2)-1);

Ex_after_lens1=t_mask1.*Ex_before_lens1;
Ey_after_lens1=t_mask1.*Ey_before_lens1;

[Ex_before_lens2,Ey_before_lens2]=...
    Propagate_waves(wavelength,n_substrate,Ex_after_lens1,Ey_after_lens1,x,y,t_substrate);



figure
imagesc(x,y,abs(Ex_after_lens1).^2)
title('Intensity after lens 1')
axis xy
axis equal
axis tight
% colormap(amir_cmap(200,'Unipolar'))
colorbar



figure
imagesc(x,y,abs(Ex_before_lens2).^2)
title('Intensity before lens 2')
axis xy
axis equal
axis tight
% colormap(amir_cmap(200,'Unipolar'))
colorbar





Ex_after_lens2=Ex_before_lens2.*t_mask2;
Ey_after_lens2=Ey_before_lens2.*t_mask2;


[Ex_at_focus,Ey_at_focus,temp,Hx_at_focus,Hy_at_focus]=...
    Propagate_waves(wavelength,n_air,Ex_after_lens2,Ey_after_lens2,x,y,z2);

I_at_focus=1/2*real(Ex_at_focus.*conj(Hy_at_focus)-Ey_at_focus.*conj(Hx_at_focus));

eta_0=376.73;
I_at_focus_norm=I_at_focus/(1/eta_0);

figure
imagesc(x,y,I_at_focus_norm)
title('Normalized intensity at focus')
axis xy
axis equal
axis tight
% colormap(amir_cmap(200,'Unipolar'))
colorbar

% figure
% imagesc(x,y,abs(Ex_at_focus).^2)
% title('Normalized intensity at focus')
% axis xy
% axis equal
% axis tight
% colormap(amir_cmap(200,'Unipolar'))
% colorbar


% mask1_phase_along_x=unwrap(angle(t_mask1(floor(length(y)/2),:)));
% mask2_phase_along_x=unwrap(angle(t_mask2(floor(length(y)/2),:)));
% figure
% plot(x,mask1_phase_along_x,x,mask2_phase_along_x)





% figure
% plot(x(2:end),(diff(mask2_phase_along_x)/dx)/(2*pi)*wavelength)
