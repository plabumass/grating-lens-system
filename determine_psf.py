import numpy as np
from fourieroptics import ScalarField, PhaseSurface

plotting = True
# coefficients are for lens in um
# # 150 um aperture
# coeffs1 = [-1.0, -2.3e-3, 2.5e-6, 7.6e-9]
# coeffs2 = [-1.1, -2.9e-3, -3.4e-6, -1.3e-8]
# RMAX = 150.0

# # 500 um aperture
# coeffs1 = [-3.09, -2.05e-3, 6.34e-7, 5.04e-10]
# coeffs2 = [-3.41, -2.75e-3, 6.92e-7, 1.2e-9]
# RMAX = 500.0                    # maximum radius of optics

# 1000 um aperture
coeffs1 = [-5.04, -1.61e-3, 2.04e-7, 7.10e-10]
coeffs2 = [-5.39, -2.50e-3, 2.14e-7, 2.92e-10]
RMAX = 1000.0                    # maximum radius of optics


def disk(x, y, radius, xoff=0.0, yoff=0.0):
    X, Y = np.meshgrid(x, y)
    X -= xoff
    Y -= yoff
    Z = np.zeros(X.shape)
    Z[abs(X*X.conjugate() + Y*Y.conjugate()) < radius**2] = 1
    return Z


def radial_phase_function(x, y, coeffs, xoff=0.0, yoff=0.0):
    """
    Phase function that defines a grating lens.
    """
    X, Y = np.meshgrid(x, y)
    X -= xoff
    Y -= yoff
    R = np.sqrt(X**2 + Y**2)
    Z = np.zeros(X.shape)
    for ix, c in enumerate(coeffs):
        Z += c * R**(ix+1)

    Z = np.mod(Z, 2*np.pi)
    return Z


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    plotting = True
    # coefficients are for lens in um
    coeffs1 = [-1.0, -2.3e-3, 2.5e-6, 7.6e-9]
    coeffs2 = [-1.1, -2.9e-3, -3.4e-6, -1.3e-8]
    RMAX = 150.0                # maximum radius of optics

    field = ScalarField(wavelength=830e-3,
                        width=1.2*2*RMAX,
                        resolution=0.2)
    X, Y = np.meshgrid(field.x, field.y)

    sigma = 40.0
    deg = 0.5
    for xdisp in (0, 10, 20, 30):
        field.apply_spherical_wave(3.75e3, x=xdisp)

        gl1 = PhaseSurface(function=lambda x, y:
                           radial_phase_function(x, y, coeffs1, yoff=0.0))

        field.apply_element(gl1, aperture=RMAX)
        if plotting and False:
            plt.figure()
            plt.pcolormesh(X, Y, field.get_intensity())
            plt.colorbar()
            plt.title("After element 1")
            plt.draw()

        field.propagate(1.0e3)
        if plotting and False:
            plt.figure()
            plt.pcolormesh(X, Y, field.get_intensity())
            plt.colorbar()
            plt.title("Before element 2")
            plt.draw()

        gl2 = PhaseSurface(function=lambda x, y:
                           radial_phase_function(x, y, coeffs2, yoff=0.0))
        field.apply_element(gl2, aperture=RMAX)

        field.propagate(1.7e3)
        if plotting:
            plt.figure()
            plt.pcolormesh(X, Y, field.get_intensity())
            plt.colorbar()
            a = 50.0
            plt.xlim(-a, a)
            plt.ylim(-a, a)
            plt.title(r"At focal plane, $\delta x$ = {:.1f} um".format(xdisp))
            plt.draw()

        plt.show()
    
